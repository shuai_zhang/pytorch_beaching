import glob
import numpy as np
import torch
import os
from model.unet_model import UNet
from PIL import Image
from dove.DOVE import read_tif_image,write_tif_image,split_tif_image,merge_tif_image,write_rgb_png
import gdal
import scipy.ndimage
import pandas as pd

if __name__ == "__main__":
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(device)
    net = UNet(n_channels=4, n_classes=1)
    net.to(device=device)
    
    net.load_state_dict(torch.load('/work_bgfs/s/szhang26/miami/model/output/199_best_model.pth', map_location=device))
    #net.load_state_dict(torch.load('record/v3.8.pth', map_location=device))
    net.eval()
    test_folder ='/work_bgfs/s/szhang26/miami/test/'
    files = glob.glob(test_folder+'/*0.tif')
    workfolder = test_folder
    if not os.path.isdir(workfolder):
        os.system('mkdir '+workfolder)
    outfolder = test_folder

    for infile in files:
        print(infile)
        folder, file_large = os.path.split(infile)
        #filelist = split_tif_image(infile,300,outfolder=workfolder)
        #df = pd.DataFrame(filelist)
        #list_name = outfolder+file_large[0:-3]+'txt'
        #print(list_name)

        #df.to_csv(list_name,index=False)
        #for file in filelist:
        if 0 ==0:
            file = infile
            path, infile = os.path.split(file)
            save_res_path = workfolder+infile[0:-3]+'extract.png'
            save_res_rgb = workfolder+infile[0:-3]+'RGB.png'
            save_res_tif = workfolder+infile[0:-3]+'extract.tif'
            img = read_tif_image(file)
            write_rgb_png(infile = file,outfile = save_res_rgb,rgb = [2,1,0])
            
            #dilated_cloud = np.where(img[6]>=0,2,2)
            #cloud_mask = np.array([[k & l for k, l in zip(i, j)] for i, j in zip(img[6].astype(np.int), arr_tmp)])
            #cloud_mask = np.where(cloud_mask == 2,0,1)
            dilated_land = img[4]
            #dilated_land=scipy.ndimage.filters.uniform_filter((img[5] ==1)*1,int(5*2+1)) != 0
            #dilated_cloud = scipy.ndimage.filters.uniform_filter((cloud_mask ==1)*1,int(2*20+1)) != 0
            no_data_idx = dilated_land ==0
            if len(no_data_idx[no_data_idx]) >= 0.99*img.shape[1]*img.shape[2]:
                print(save_res_path)
                print('No enough valid points...')
                pred = np.zeros((img.shape[1],img.shape[2]))
            else:
                print(save_res_path)
                arr_cal = img.copy()[0:4]
                arr_cal = np.where(dilated_land==1,arr_cal,0)
                arr_cal = arr_cal.reshape(1, 4, img.shape[1], img.shape[2])
                img_tensor = torch.from_numpy(arr_cal)
                img_tensor = img_tensor.to(device=device, dtype=torch.float32)
                pred = net(img_tensor)
                pred = np.array(pred.data.cpu()[0])[0]
                pred[pred >= 0.5] = 1
                pred[pred < 0.5] = 0
                pred[no_data_idx] = 0
           
            out_arr = img.copy()
            if len(pred[pred>0]) >0:
                out_arr[2][pred > 0] = 1
                out_arr[1][pred > 0] = 0
                out_arr[0][pred > 0] = 0
            write_rgb_png(arr=out_arr,outfile=save_res_path,rgb=[2,1,0])
            print(out_arr.shape)
            print(img.shape)
            ds = gdal.Open(file)
            #write_tif_image(out_arr,ds,save_res_tif)
            
  
