#!/bin/bash
#SBATCH --job-name=fast_beaching
#SBATCH -a 0-100
#SBATCH --time=1:00:00
#SBATCH --output=log.%A_%a.txt 
#SBATCH --partition=tfawcett
#SBATCH --qos=fawcett_access
#SBATCH --mem=40000


#set environment
source /shares/cms_optics/etc/opticsrc
. /apps/anaconda3/5.0.0/etc/profile.d/conda.sh
conda activate pytorch

ls /work_bgfs/s/szhang26/miami/tif/{20190406_106f,20190412_1044,20190414_0f4d,20190414_1105,20190418_1014,20190421_0f34,20190422_0f4d,20190424_0f32,20190428_1051,20190503_0f2e,20190503_1004,20190505_100c,20190514_0e20,20190521_1032,20190522_1002,20190525_1014,20190528_1058}*S.tif > tif.list

filelist='./tif.list'

pres+=($(cat $filelist))
printf "%s\n" "Working on file(s): ${pres[$SLURM_ARRAY_TASK_ID]}"

python predict_fast.py -i ${pres[$SLURM_ARRAY_TASK_ID]}
