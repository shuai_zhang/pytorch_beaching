#!/usr/bin/env python3
import glob
import numpy as np
import torch
import os
from model.unet_model import UNet
from PIL import Image
from dove.DOVE import read_tif_image,write_tif_image,split_tif_image,merge_tif_image,write_rgb_png
import gdal
import scipy.ndimage
import pandas as pd

def predict_extraction(infile,outfolder = None):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(device)
    net = UNet(n_channels=4, n_classes=1)
    net.to(device=device)
    #net.load_state_dict(torch.load('record/v3.8.pth', map_location=device))
    net.load_state_dict(torch.load('/work_bgfs/s/szhang26/miami/model/output/272_best_model.pth', map_location=device))
    net.eval()
    #files = glob.glob('/work_bgfs/s/szhang26/miami/tif/2019*.tif')
    nrows,ncols = read_tif_image(infile)[0].shape
    if outfolder == None:
        outfolder = '/work_bgfs/s/szhang26/miami/pred_fast/'
    print(infile)
    folder, file_large = os.path.split(infile)

    workfolder = '/work_bgfs/s/szhang26/miami/work_'+file_large[0:-4]+'/'
    if not os.path.isdir(workfolder):
        os.system('mkdir '+workfolder)

    filelist = split_tif_image(infile,500,outfolder=workfolder)
    df = pd.DataFrame(filelist)
    list_name = outfolder+file_large[0:-3]+'txt'
    print(list_name)

    df.to_csv(list_name,index=False)
    for file in filelist:
        print(file)
        path, infile = os.path.split(file)
        save_res_path = workfolder+infile[0:-3]+'extract.png'
        save_res_rgb = workfolder+infile[0:-3]+'RGB.png'
        save_res_tif = workfolder+infile[0:-3]+'extract.tif'
        img = read_tif_image(file)
        write_rgb_png(infile = file,outfile = save_res_rgb,rgb=[2,1,0])
            
        dilated_land = img[4]
        no_data_idx = (dilated_land ==0)
        if len(no_data_idx[no_data_idx]) >= 0.99*img.shape[1]*img.shape[2]:
            print(save_res_path)
            print('No enough valid points...')
            pred = np.zeros((img.shape[1],img.shape[2]))
        else:
            print(save_res_path)
            arr_cal = img.copy()[0:4]
            arr_cal = np.where(dilated_land==1,arr_cal,0)
            arr_cal = arr_cal.reshape(1, 4, img.shape[1], img.shape[2])
            img_tensor = torch.from_numpy(arr_cal)
            img_tensor = img_tensor.to(device=device, dtype=torch.float32)
            pred = net(img_tensor)
            pred = np.array(pred.data.cpu()[0])[0]
            pred[pred >= 0.5] = 1
            pred[pred < 0.5] = 0
            pred[no_data_idx] = 0
           
        out_arr = img.copy()
        if len(pred[pred>0]) >0:
            out_arr[2][pred > 0] = 1
            out_arr[1][pred > 0] = 0
            out_arr[0][pred > 0] = 0
        write_rgb_png(arr=out_arr,outfile=save_res_path,rgb=[2,1,0])
        print(img.shape)
        ds = gdal.Open(file)
        write_tif_image(out_arr,ds,save_res_tif)
            
    outfile = outfolder+file_large

    fin = open(list_name,'r')
    fout = open(list_name[0:-3]+'extract.txt','w')
    data = fin.read()
    data = data.replace('tif','extract.tif')
    fout.write(data)
    fout.close()
    fin.close()
    merge_tif_image(list_name[0:-3]+'extract.txt',outfile=outfile)
    out_arr = read_tif_image(outfile)
    out_arr_cut = np.zeros((3,nrows,ncols))
    out_arr_cut = out_arr[:,0:nrows,0:ncols]

    write_rgb_png(arr = out_arr_cut,outfile = outfile[0:-3]+'extract.png',rgb=[2,1,0])
    write_rgb_png(infile = folder+'/'+file_large,outfile = outfile[0:-3]+'RGB.png',rgb=[2,1,0])
    #os.system('rm -rf '+workfolder) 

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Predict Beaching Sargassum')
    parser._action_groups.pop()
    required = parser.add_argument_group('required arguments')
    optional = parser.add_argument_group('optional arguments')

    required.add_argument('-i','--infile',type=str,help='infile',required=True)
    optional.add_argument('-o', '--outfolder', help='outfolder')
    args = parser.parse_args()

    predict_extraction(infile = args.infile, outfolder = args.outfolder)



 
