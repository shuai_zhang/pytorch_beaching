import torch
import os
import glob
from torch.utils.data import Dataset
import random
from PIL import Image
import numpy as np
from dove.DOVE import read_tif_image
import scipy.ndimage

#05/10/2021 changed from cyano

class ISBI_Loader(Dataset):
    def __init__(self, data_path):
        self.data_path = data_path
        self.imgs_path = glob.glob(os.path.join(data_path, 'image/*.tif'))

    def __getitem__(self, index):
        image_path = self.imgs_path[index]
        label_path = image_path.replace('image', 'label')[0:-3]+'extract.png'
        arr_img = read_tif_image(image_path)
        if os.path.isfile(label_path):            
            arr_label = np.array(Image.open(label_path))
            label = np.array([(arr_label[:,:,0]==255)&(arr_label[:,:,1]==0)&(arr_label[:,:,2]==0)])[0]*1.0
        else:
            label = np.zeros((arr_img.shape[1], arr_img.shape[2]))

        dilated_land = arr_img[4]
        #dilated_land=scipy.ndimage.filters.uniform_filter((arr_img[5] ==1)*1,int(5*2+1)) != 0
        image = np.where(dilated_land>0,arr_img,0)[0:4]
        image = image.reshape(4, image.shape[1], image.shape[2])
        label = label.reshape(1, label.shape[0], label.shape[1])
        label = np.where(label<1,0,1)
        #if label.max() > 1:
        #    label = label / 255
        '''
        flipCode = random.choice([-1, 0, 1, 2])
        if flipCode != 2:
            image = self.augment(image, flipCode)
            label = self.augment(label, flipCode)
        '''
        return image, label

    def __len__(self):
        return len(self.imgs_path)


if __name__ == "__main__":

    isbi_dataset = ISBI_Loader("/work_bgfs/s/szhang26/miami/train/")
    print('Length of dataset', len(isbi_dataset))
    train_loader = torch.utils.data.DataLoader(dataset=isbi_dataset,
                                               batch_size=2,
                                               shuffle=True)

    for image, label in train_loader:
        print(image)
        print(label)
