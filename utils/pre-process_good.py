#/usr/bin/env python3

#for transferring the old training dataset in envi format to tiff format

import os
import numpy as np
from PIL import Image
import xml.etree.ElementTree as ET
import pandas as pd
import gdal
import glob
from dove.read_envi import read_envi_data
from dove.DOVE import read_tif_image,write_rgb_png,write_tif_image

folder = '/work_bgfs/s/szhang26/miami/train/good/'
files = glob.glob(folder+'*0.tif')

for file in files:
    class_file = file[0:-3]+'class.hdr'
    mask_file = file[0:-3]+'mask.hdr'
    rgb_file = file[0:-3]+'RGB.png'
    print(mask_file)
    if os.path.isfile(class_file):
        arr_idx = read_envi_data(class_file)[0]

    else:
        class_file = file[0:-3]+'extract.png'
        arr_label = np.array(Image.open(class_file))
        arr_idx = np.array([(arr_label[:,:,0]==255)&(arr_label[:,:,1]==0)&(arr_label[:,:,2]==0)])[0]*1.0

    arr_rgb = np.array(Image.open(rgb_file))
    #modify pixel values based on the masks
    if os.path.isfile(mask_file) == True:
        arr_mask = read_envi_data(mask_file)[0]
        print('mask found. ')
        outfile = file[0:-3]+'extract.png'
        arr_idx = np.where(arr_mask==2,0,arr_idx)
        arr_idx = np.where(arr_mask==1,1,arr_idx)
       
         
        arr_out = np.zeros((arr_mask.shape[0],arr_mask.shape[1],3))
        
        arr_out[:,:,0] = np.where(arr_idx==1,arr_idx*255,arr_rgb[:,:,0])
        arr_out[:,:,1] = np.where(arr_idx==1,0,arr_rgb[:,:,1])
        arr_out[:,:,2] = np.where(arr_idx==1,0,arr_rgb[:,:,2])
        
        pic=Image.fromarray(np.array(arr_out, dtype=np.uint8),mode='RGB')
        pic.save(outfile)


