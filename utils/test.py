from dove.DOVE import read_tif_image,write_tif_image,write_rgb_png,plot_ndvi
from PIL import Image
import numpy as np

infile = '/work_bgfs/s/szhang26/miami/test/20190406_1010_AnalyticMS.500_1000.tif'

#dove wavelength
n1=485
n2=545
n3=630
n4=820 

def cal_ffai(r1,r2,r3,n1,n2,n3):
    ffai = r2 - (r1 + (r3 - r1) / (n3 - n1) * (n2 - n1))
    return ffai

arr = read_tif_image(infile)
r1 = arr[1]
r2 = arr[2]
r3 = arr[3]

palette_png='/shares/cms_optics/py/satpack/images/fa_density_colorbar.png'
ffai = cal_ffai(r1,r2,r3,n2,n3,n4)
print(ffai)
cmin = -0.01
cmax = 0.03
ffai[ffai<cmin] = cmin
ffai[ffai>cmax] = cmax
ffai = (ffai-cmin)/(cmax-cmin)*235

junk=Image.open(palette_png)
palette=junk.getpalette()
pic=Image.fromarray(np.array(ffai,dtype=np.uint8),mode='P')
pic.putpalette(palette)
pic.save('ffai.png')




