import numpy as np
import sys
from PIL import Image
import scipy
from scipy import ndimage
from DOVE import read_tif_image,write_rgb_png,write_tif_image
import os
import gdal
folder = '/work_bgfs/s/szhang26/CRE/train/image/pretrain/bad/'
for root, dirs, files in os.walk(folder):
    for file in files:
        if file.endswith('0.tif'):
            infile = root+file
            outfile = infile[0:-3]+'ndvi.png'
            testfile = infile[0:-3]+'test.png'
            print(outfile)
            ndvi = read_tif_image(infile)[4]
            palette_png='/shares/cms_optics/py/satpack/images/fa_density_colorbar.png'
            arr_out = np.zeros((3,300,300))

            for k in range(0,3):
                arr_out[k] = ndvi
           
            
            cmin = 0.03
            cmax=0.20
            high = 235
            img = np.where(ndvi<cmin,cmin,ndvi)
            img = np.where(img>cmax,cmax,img)
            img= (img - cmin)/(cmax-cmin)*high

            #img = np.where(ndvi==0,0,img)                 
            #img = 255*(np.log10(img/cmin) / np.log10(cmax/cmin))+0.5

            junk=Image.open(palette_png)
            palette=junk.getpalette()

            pic=Image.fromarray(np.array(img,dtype=np.uint8),mode='P')
            pic.putpalette(palette)
            pic.save(outfile)

            

