#!/bin/bash
#SBATCH --job-name=CY
#SBATCH --time=60:00:00
#SBATCH --output=output.%j
#SBATCH --partition=tfawcett
#SBATCH --qos=fawcett_access
#SBATCH --gres=gpu:1
#SBATCH --mem=25600


#set environment
source /shares/cms_optics/etc/opticsrc
. /apps/anaconda3/5.0.0/etc/profile.d/conda.sh
conda activate pytorch

#python cut_image.py
python add_mask.py
