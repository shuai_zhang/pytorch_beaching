
import gdal
import matplotlib.pyplot as plt
import numpy as np
from dove.DOVE import read_tif_image,write_rgb_png

def plot_spec(t_list,b_list,labels=None,outfile='png/spectra.png'):
    x = range(0,len(t_list))   
    if labels!=None:
        x=labels
    fig, ax0 = plt.subplots(1,1,figsize=(4,3.6))
    
    #plt.subplots_adjust(wspace=0.45, hspace=None)
    ax1 = ax0.twinx()
    fsize = 14
    ax1.tick_params(axis='both', which='major', labelsize=fsize)
    ax0.tick_params(axis='both', which='major', labelsize=fsize) 
    ln1=ax0.plot(x, t_list,color = (205/255, 133/255, 63/255),marker='o',linestyle='-',label='Sargassum')
    ln2=ax0.plot(x, b_list,color = (0/255, 0/255, 255/255),marker='o',linestyle='-',label='water')
    ln3=ax1.plot(x, np.array(t_list)-np.array(b_list),color = (0/255, 0/255, 0/255),marker='s',markerfacecolor='none',linestyle='--',label='diff')
    lns = ln1+ln2+ln3

    labs = [l.get_label() for l in lns]
    #ax0.legend(lns, labs,loc='center',bbox_to_anchor=(0.55,0.72),frameon=False,fontsize = 10)
    ax0.set_xlabel('$\lambda$'+" (nm)",fontsize = fsize)
    ax0.set_ylabel(r"TOA "+'$\it{R}$',fontsize = fsize)
    ax1.set_ylabel(r"Δ"+'$\it{R}$', fontsize = fsize)
    
    plt.savefig(outfile,bbox_inches = "tight")
    
def extract_values(arr,targ,ref):
    arr_targ = arr[:,targ[0],targ[1]]
    arr_ref = arr[:,ref[0],ref[1]]
    return arr_targ,arr_ref

def plot_png(infile,targ,ref,outfile='png/rgb.png',imgtype='RGB',buff=3):
    arr = read_tif_image(infile)[0:4]
    arr_out = np.zeros((3,arr.shape[1],arr.shape[2]))
    if imgtype == 'RGB':
        arr_out[0] = arr[2]
        arr_out[1] = arr[1]
        arr_out[2] = arr[0]
    elif imgtype =='FRGB':
        arr_out[0] = arr[2]
        arr_out[1] = arr[3]
        arr_out[2] = arr[0]

    for x in range(-buff,buff):
        for y in range(-buff,buff):    
            arr_out[:,targ[0]+x,targ[1]+y+1] = [0,0,1]
            arr_out[:,ref[0]+x,ref[1]+y+1] = [0,0,0]
    write_rgb_png(arr=arr_out,outfile=outfile,rgb=[0,1,2])

#strong sarg
infile = '/work_bgfs/s/szhang26/miami/merge/20190512_0f49_AnalyticMS.tif'
targ = [2642,679]
ref = [2631,758]

infile = '/work_bgfs/s/szhang26/miami/merge/20190415_1052_AnalyticMS.tif'
#strong sargs
#targ = [2652,679]
#ref = [2597,774]

#weak sargs
targ = [1666,830]
ref = [1697,824]

#dark water
#targ = [3320,801]
#ref = [3210,851]


arr = read_tif_image(infile)
plot_png(infile,targ,ref,buff=2,imgtype='RGB')
arr_t,arr_ref = extract_values(arr,targ,ref)
plot_spec(arr_t[0:4],arr_ref[0:4],labels = [485,545,630,820])





