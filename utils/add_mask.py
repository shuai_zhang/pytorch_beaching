from PIL import Image
import gdal
import glob
import numpy as np
from dove.DOVE import read_tif_image,write_tif_image
from dove.read_envi import read_envi_data
from scipy import ndimage

infolder = '/work_bgfs/s/szhang26/miami/merge/'
infiles = glob.glob(infolder+'201909*S.tif')
mask = '/work_bgfs/s/szhang26/miami/mask/beach_mask.hdr'
arr_mask = read_envi_data(mask)[0]

for file in infiles:
    print(file)
    ds = gdal.Open(file)
    arr = read_tif_image(file)
    height = min(arr[0].shape[0],arr_mask.shape[0])
    width = min(arr[0].shape[1],arr_mask.shape[1])

    arr_out = np.zeros((5,height,width))

    for k in range(0,4):
        arr_out[k,0:height,0:width] = arr[k,0:height,0:width]
    arr_out[4,0:height,0:width] = arr_mask[0:height,0:width]

    write_tif_image(arr_out,ds,file.replace('merge','tif'))
    
